app.controller('forumController',['$scope', '$http', '$cookies', function($scope, $http, $cookies){

    $scope.allMessages = [] ;
    $scope.userMessages = [] ;
    $scope.message = "" ;
    $scope.url = "http://localhost:8000/api/forum" ;

    $scope.Intialize = function() {
        var user_connected = $cookies.get('id_utilisateur') ;

        if ( user_connected == "" ) {
            window.location = "#!" ;
        } else {

            var user = {
                "id_user": user_connected
            }

            //Récupération des messages réçus
            $http.post($scope.url+'/message-all', JSON.stringify(user)).then(function (response) {
                
                $scope.allMessages = response.data.data ;
                
            }, function (response) {
                
                $scope.msg = "Service not Exists";
                $scope.statusval = response.status;
                $scope.statustext = response.statusText; 
                $scope.headers = response.headers();
                
            });

            //Récupération des messages envoyés
            $http.post($scope.url+'/message-user', JSON.stringify(user)).then(function (response) {
                
                $scope.userMessages = response.data.data ;
                
            }, function (response) {
                
                $scope.msg = "Service not Exists";
                $scope.statusval = response.status;
                $scope.statustext = response.statusText; 
                $scope.headers = response.headers();
                
            });

        }
    }

    $scope.sendMessage = function() {

        var user_connected = $cookies.get('id_utilisateur') ;
        var message_user = {
            "id_user": user_connected,
            "message": $scope.message
        }

        //Envoi du message
        $http.post($scope.url+'/message-add', JSON.stringify(message_user)).then(function (response) {
                
            $scope.Intialize() ;
            $scope.message = "" ;
            
        }, function (response) {
            
            $scope.msg = "Service not Exists";
            $scope.statusval = response.status;
            $scope.statustext = response.statusText; 
            $scope.headers = response.headers();
            
        });

    }

}]) ;