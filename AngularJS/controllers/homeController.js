app.controller('homeController', ['$scope', '$http', '$cookies', function($scope, $http, $cookies){

    //Controller de la page home.html

    // Déclaration de la variable qui contiendra le pseudo
    $scope.pseudo = "" ;
    $scope.url = "http://localhost:8000/api/forum" ;

    $scope.Initialize = function() {
        //alert("Essaie") ;
    }

    $scope.connectForum = function() {
        
        if ( $scope.pseudo == "" ) {

            Swal.fire({
                title: 'Veuillez renseigner un pseudonyme svp !',
                showClass: {
                  popup: 'animated fadeInDown faster'
                },
                hideClass: {
                  popup: 'animated fadeOutUp faster'
                }
            });

        } else {

            var pseudo_user = {
                "pseudo": $scope.pseudo
            }

            var user_connected = $cookies.get('id_utilisateur') ;

            if ( $scope.pseudo == user_connected ) {

                $cookies.put('id_utilisateur', $scope.pseudo);
                window.location = "#!forum" ;

            } else {
                
                $http.post($scope.url+'/pseudo-user', JSON.stringify(pseudo_user)).then(function (response) {

                    //alert( response.data.data[0]["id_utilisateur"] )
    
                    if (response.data.error == true) {
    
                        Swal.fire({
                            title: response.message,
                            showClass: {
                              popup: 'animated fadeInDown faster'
                            },
                            hideClass: {
                              popup: 'animated fadeOutUp faster'
                            }
                        });
    
                    } else {
                        $cookies.put('id_utilisateur', response.data.data[0]["id_utilisateur"]);
                        window.location = "#!forum"
                    }
                    
                    
                }, function (response) {
                    
                    $scope.msg = "Service not Exists";
                    
                    $scope.statusval = response.status;
                    
                    $scope.statustext = response.statusText;
                    
                    $scope.headers = response.headers();
                    
                });
            }

        }

    }

}]) ;