$.when(
    $.getScript( "angular.min.js" ),
    $.getScript( "angular-route.min.js" ),
    $.getScript( "controllers/homeController.js" ),
    $.Deferred(function( deferred ){
        $( deferred.resolve );
    })
).done(function(){

    //place your code here
    console.log("the scripts are all loaded") ;

});