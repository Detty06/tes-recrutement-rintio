app.config( function($routeProvider) {

    $routeProvider
        .when('/', {
            templateUrl: 'pages/home.html',
            controller: 'homeController'
        })
        .when('/forum', {
            templateUrl: 'pages/forum.html',
            controller: 'forumController'
        })
        .otherwise({ redirectTo : '/' });

}) ;