var db = require('../config/db');

module.exports.message_user = function(req,res){

    var id_user = req.body.id_user;

    db.query('SELECT * FROM message WHERE id_utilisateur = ?', [id_user],  function (error, results, fields) {
        
        if ( error ) {

            res.json({
                error: true,
                message: error
            }) ;

        } else {

            res.json({
                error: false,
                message: error,
                data: results
            }) ;
        }
    });
}

module.exports.message_all = function(req,res){


    var id_user = req.body.id_user;

    db.query('SELECT * FROM message INNER JOIN utilisateur WHERE message.id_utilisateur = utilisateur.id_utilisateur AND message.id_utilisateur <> ? ', [id_user],  function (error, results, fields) {
        
        if ( error ) {

            res.json({
                error: true,
                message: error
            }) ;

        } else {

            res.json({
                error: false,
                message: error,
                data: results
            }) ;
        }
    });
}

module.exports.message_add = function(req,res){

    var today = new Date();
    var 
        id_user = req.body.id_user,
        message_user = req.body.message,
        date_message = today
    ;

    var message = {
        "id_utilisateur": id_user,
        "message": message_user,
        "date_message": date_message
    }

    db.query('INSERT INTO message SET ?', message ,  function (error, results, fields) {
        
        if ( error ) {

            res.json({
                error: true,
                message: error
            }) ;

        } else {

            res.json({
                error: false,
                message: "Message envoyé !"
            }) ;
        }
    });
}

