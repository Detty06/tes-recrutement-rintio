var express    = require("express");
var bodyParser = require('body-parser');

var app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

/* Controllers */
var userController = require('./controllers/user-controller');
var messageController = require('./controllers/message-controller');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

/* Routes */
app.post('/api/forum/pseudo-user', userController.pseudo);
app.post('/api/forum/message-all', messageController.message_all);
app.post('/api/forum/message-user', messageController.message_user);
app.post('/api/forum/message-add', messageController.message_add);



/* Listen Port */
app.listen(8000);